
use std::fs::File;
use std::str::FromStr;
use std::io;
use std::io::{Read, BufRead, BufReader};
use std::time;
use std::sync::mpsc;

use clap::{Arg, App};

use rayon::ThreadPoolBuilder;

use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;

use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;

use tui::backend::TermionBackend;
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Axis, Block, Borders, Chart, Dataset, Marker, Widget};
use tui::Terminal;

mod point;
use point::Point;
use point::ClusterPoint;
use point::CoordinateAware;

fn main() {
    let cli_args = App::new("K-Means clusterizer implementation")
                        .version("0.1.0")
                        .author("Oleg `Kanedias` Chernovskiy <kanedias@keemail.me>")
                        .about("Computes K-Means over a file with 2D points in TSV format and outputs the result")
                        .arg(Arg::with_name("clusters")
                                .value_name("number")
                                .long("clusters")
                                .short("c")
                                .required(true)
                                .takes_value(true)
                                .help("Number of clusters to converge to"))
                        .arg(Arg::with_name("threads")
                                .value_name("number")
                                .long("threads")
                                .short("t")
                                .required(false)
                                .takes_value(true)
                                .help("Number of threads to use. Defaults to 1"))
                        .arg(Arg::with_name("verbose")
                                .long("verbose")
                                .short("v")
                                .required(false)
                                .help("Verbose output"))
                        .arg(Arg::with_name("draw")
                                .long("draw")
                                .short("d")
                                .required(false)
                                .help("Draw chart, works only if there are 2 dimensions"))
                        .arg(Arg::with_name("input-file")
                                .required(true)
                                .help("Input data to compute K-Means from."))
                        .get_matches();
                        

    let input_filename = cli_args.value_of("input-file").expect("input file argument must be present");
    let cluster_count = cli_args.value_of("clusters").expect("cluster count must be set");
    let thread_count = cli_args.value_of("threads").unwrap_or("1");
    let should_draw = cli_args.is_present("draw");

    let input_file = File::open(input_filename).expect("input file must be readable");
    let reader = BufReader::new(input_file);

    // read file with point data
    let mut dimensions_count = 0;
    let mut data_grid: Vec<Point> = Vec::new();
    let read_start = time::Instant::now();
    for line in reader.lines() {
        let line = line.expect("file must have readable lines");
        if dimensions_count == 0 {
            dimensions_count = line.split_whitespace().count();
        }

        let features = line.split_whitespace();
        let mut point = Point::new(dimensions_count);
        for (idx, feat) in features.enumerate() {
            point.features[idx] = f64::from_str(feat).expect("feature must be a number");
        }
        data_grid.push(point);
    }
    println!("Data file read successfully, took {} milliseconds", read_start.elapsed().as_millis());

    // shadow string variables by their number counterparts
    let cluster_count = usize::from_str(cluster_count).expect("cluster count must be a number");
    if cluster_count < 2 {
        panic!("Cluster count must be higher than 1");
    }

    let thread_count = usize::from_str(thread_count).expect("thread count must be a number");
    if thread_count < 1 {
        panic!("Thread count must be at least 1");
    }

    // initialize global thread pool
    ThreadPoolBuilder::new()
        .num_threads(thread_count)
        .thread_name(|idx| format!("kmeans-compute-{}", idx))
        .build_global().expect("Global thread pool must be initialized");

    let centroids = perform_k_means(&data_grid, cluster_count, thread_count, dimensions_count);

    if should_draw && dimensions_count == 2 {
        draw_grid(data_grid, centroids);
    }
}

fn perform_k_means(data_grid: &Vec<Point>, cluster_count: usize, thread_count: usize, dimensions_count: usize) -> Vec<ClusterPoint> {
    // let mut rng = SmallRng::from_entropy();
    let mut rng = SmallRng::from_seed([42; 16]);

    // allocate clusters in a way that resizing vectors won't be needed
    let mut centroids = vec![ClusterPoint::new(dimensions_count); cluster_count];

    // chunk size should be greater than quotient, or chunk number will be greater than thread number
    // consider 100 / 3 = 33, causing chunks to be [33, 33, 33, 1]
    let chunk_size = (data_grid.len() as f64 / thread_count as f64).ceil() as usize;

    // make centroids pick one point as a starting coordinate
    for centroid in centroids.iter_mut() {
        let idx = rng.gen_range(0, data_grid.len());
        centroid.reset(&data_grid[idx]);
    }
    
    // now, in a loop, repeat until convergence
    let kmeans_start = time::Instant::now();
    let mut iterations = 0;
    println!("Commencing K-Means clustering, searching {} clusters in {} points with {} dimensions", cluster_count, data_grid.len(), dimensions_count);

    loop {
        // store old centroids to calculate diff later
        let old_centroids = centroids.clone();

        // create channel that will receive computation results back
        let (tx, rx) = mpsc::sync_channel(thread_count);

        // Create scoped execution. Variables defined in this scope can't outlive it
        // All threads requested in this scope are guaranteed to be completed after it ends
        rayon::scope(|s| {
            for chunk in data_grid.chunks(chunk_size) {
                // make references that can be moved on a spawned thread stack
                let old_centroids_threadlocal = &old_centroids;
                let tx = tx.clone();

                s.spawn(move |_| {
                    let mut centroids_for_chunk = vec![ClusterPoint::new(dimensions_count); cluster_count];
                    for point in chunk.iter() {
                        // for each point detect centroid nearest to it
                        let (cluster_idx, _) = old_centroids_threadlocal.iter()
                                .map(|centroid| centroid.coordinates.quad_distance(&point))
                                .enumerate()
                                .min_by(|(_, distance1), (_, distance2)| distance1.partial_cmp(&distance2).expect("floats are sortable"))
                                .expect("results are sorted");

                        // we are computing sum of centroid coordinates for each chunk, they will be divided later, all at once
                        centroids_for_chunk[cluster_idx] += point;
                    }
                    tx.send(centroids_for_chunk).expect("Send computation results back to main loop");
                });
            }
        });

        // update centroids, sum all coordinates from chunks
        centroids.iter_mut().for_each(|centroid| centroid.clear());
        for _ in 0..thread_count {
            let centroids_for_chunk = rx.recv().expect("chunk must be processed in scope");
            for cluster_idx in 0..cluster_count {
                centroids[cluster_idx] += &centroids_for_chunk[cluster_idx];
            }
        }

        // make centroid coordinates an arithmetic mean of all points it consists of
        centroids.iter_mut().for_each(|centroid| {
            // edge-case, probably user chose too much clusters?
            if centroid.cluster_size == 0 {
                println!("Warning, got an empty cluster, reassigning to random point...");
                let idx = rng.gen_range(0, data_grid.len());
                centroid.reset(&data_grid[idx]);
            }

            centroid.normalize_coordinates();
        });

        // compute how coordinates of centroids changed, compared to old coordinates
        // basically, take a sum of distances between corresponding old and new centroids
        let mut diff = 0.0;
        for cluster_idx in 0..cluster_count {
            diff += old_centroids[cluster_idx].coordinates.distance(&centroids[cluster_idx].coordinates);
        }

        // if the diff is low then clusters converged, break out of the loop
        if diff < 0.00001 {
            break;
        }

        iterations += 1;
    }

    println!("K-Means converged after {} iterations, took {} milliseconds", iterations, kmeans_start.elapsed().as_millis());
    for (cluster_idx, centroid) in centroids.iter().enumerate() {
        println!("Cluster {}: centroid {}", cluster_idx, centroid);
    }

    return centroids;
}

fn draw_grid(data_grid: Vec<Point>, centroids: Vec<ClusterPoint>) {
    let stdout_raw = io::stdout().into_raw_mode().expect("Terminal must support raw mode");
    let screen = TermionBackend::new(AlternateScreen::from(stdout_raw));
    let mut terminal = Terminal::new(screen).expect("Terminal must have rich color support");

    let min_x = data_grid.iter().map(|point| point.features[0]).min_by(|x1, x2| x1.partial_cmp(&x2).expect("floats are sortable")).unwrap();
    let max_x = data_grid.iter().map(|point| point.features[0]).max_by(|x1, x2| x1.partial_cmp(&x2).expect("floats are sortable")).unwrap();
    let min_y = data_grid.iter().map(|point| point.features[1]).min_by(|y1, y2| y1.partial_cmp(&y2).expect("floats are sortable")).unwrap();
    let max_y = data_grid.iter().map(|point| point.features[1]).max_by(|y1, y2| y1.partial_cmp(&y2).expect("floats are sortable")).unwrap();

    let data_conv: Vec<(f64, f64)> = data_grid.iter().map(|point| (point.features[0], point.features[1])).collect();
    let centroids_conv: Vec<(f64, f64)> = centroids.iter().map(|point| &point.coordinates).map(|point| (point.features[0], point.features[1])).collect();

    terminal.draw(|mut frame| {
        let size = frame.size();

        Chart::default()
                .block(
                    Block::default()
                        .title("Data grid")
                        .title_style(Style::default().fg(Color::Cyan).modifier(Modifier::BOLD))
                        .borders(Borders::ALL),
                )
                .x_axis(
                    Axis::default()
                        .title("X Axis")
                        .style(Style::default().fg(Color::Gray))
                        .labels_style(Style::default().modifier(Modifier::ITALIC))
                        .bounds([min_x, max_x])
                        .labels(&[
                            &format!("{}", min_x),
                            &format!("{}", max_x),
                        ]),
                )
                .y_axis(
                    Axis::default()
                        .title("Y Axis")
                        .style(Style::default().fg(Color::Gray))
                        .labels_style(Style::default().modifier(Modifier::ITALIC))
                        .bounds([min_y, max_y])
                        .labels(&[
                            &format!("{}", min_y),
                            &format!("{}", max_y),
                        ]),
                )
                .datasets(&[
                    Dataset::default()
                        .name("Data")
                        .marker(Marker::Dot)
                        .style(Style::default().fg(Color::Cyan))
                        .data(&data_conv),
                    Dataset::default()
                        .name("Centroids")
                        .marker(Marker::Dot)
                        .style(Style::default().fg(Color::Yellow))
                        .data(&centroids_conv),
                ])
                .render(&mut frame, size);
    }).expect("Must be able to render");

    // wait for next character
    io::stdin().bytes().next();
}