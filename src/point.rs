use std::ops;
use std::fmt::Display;

pub trait CoordinateAware {
    fn vector_addition(&mut self, other: &Self);
    fn divide_scalar(&mut self, rhs: f64);
    fn quad_distance(&self, other: &Self) -> f64;

    fn distance(&self, other: &Self) -> f64 {
        return self.quad_distance(other).sqrt();
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Point {
    pub features: Vec<f64>
}

impl Point {
    pub fn new(dimensions: usize) -> Self {
        Self { features: vec![0.0; dimensions] }
    }
}

impl CoordinateAware for Point {
    fn vector_addition(&mut self, other: &Self) {
        for idx in 0..self.features.len() {
            self.features[idx] += other.features[idx];
        }
    }

    fn divide_scalar(&mut self, rhs: f64) {
        for idx in 0..self.features.len() {
            self.features[idx] /= rhs;
        }
    }

    fn quad_distance(&self, other: &Point) -> f64 {
        let mut sum = 0.0;
        for idx in 0..self.features.len() {
            sum += (self.features[idx] - other.features[idx]).powf(2.0)
        }
        return sum;
    }
}

#[derive(Clone, Debug)]
pub struct ClusterPoint {
    pub coordinates: Point,
    pub cluster_size: usize
}

impl ClusterPoint {
    pub fn new(dimensions: usize) -> Self {
        Self { coordinates: Point::new(dimensions), cluster_size: 0 }
    }

    pub fn clear(&mut self) {
        self.coordinates = Point::new(self.coordinates.features.len());
        self.cluster_size = 0;
    }

    pub fn reset(&mut self, point: &Point) {
        self.coordinates = point.clone();
        self.cluster_size = 1;
    }

    pub fn normalize_coordinates(&mut self) {
        self.coordinates.divide_scalar(self.cluster_size as f64);
    }
}

impl Display for ClusterPoint {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.coordinates.features.len() <= 5 {
            write!(f, "coordinates: {:?}, cluster size: {}", self.coordinates.features, self.cluster_size)?;
        } else {
            write!(f, "coordinates: [")?;
            for idx in 0..3 {
                write!(f, "{}, ", self.coordinates.features[idx])?;
            }
            write!(f, " ..., {}]", self.coordinates.features.last().expect("len was checked"))?;
            write!(f, ", cluster size: {}", self.cluster_size)?;
        }
        return Result::Ok(());
    }

}

impl CoordinateAware for ClusterPoint {
    fn vector_addition(&mut self, other: &Self) {
        self.coordinates.vector_addition(&other.coordinates);
    }

    fn divide_scalar(&mut self, rhs: f64) {
        self.coordinates.divide_scalar(rhs);
    }

    fn quad_distance(&self, other: &ClusterPoint) -> f64 {
        return self.coordinates.quad_distance(&other.coordinates);
    }
}

impl ops::AddAssign<&Point> for &mut ClusterPoint {

    fn add_assign(&mut self, rhs: &Point) {
        (*self).add_assign(rhs);
    }
}

impl ops::AddAssign<&Point> for ClusterPoint {

    fn add_assign(&mut self, rhs: &Point) {
        self.coordinates.vector_addition(rhs);
        self.cluster_size += 1;
    }
}

impl ops::AddAssign<&ClusterPoint> for ClusterPoint {

    fn add_assign(&mut self, rhs: &ClusterPoint) {
        self.vector_addition(rhs);
        self.cluster_size += rhs.cluster_size;
    }
}

impl ops::DivAssign<f64> for &mut ClusterPoint {
    
    fn div_assign(&mut self, rhs: f64) { 
        self.coordinates.divide_scalar(rhs);
    }
}