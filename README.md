K-Means in Rust
===============

[![build status](https://gitlab.com/Kanedias/k-means-rust/badges/master/pipeline.svg)](https://gitlab.com/Kanedias/k-means-rust/pipelines)
[![License](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![Universal binary](https://img.shields.io/badge/Universal-ELF-brightgreen.svg)](https://gitlab.com/Kanedias/k-means-rust/-/jobs/artifacts/master/download?job=release+static)

![Sample output image](https://i.imgur.com/5Aw0tuQ.png)

Parallel K-Means implementation written in Rust.  
See [here][1] for the definition of a problem and possible algorithm implementations.

Building
========

In case you don't have Rust toolchain installed, use [Rustup][2] to quickly setup it.
Then just run `cargo build --release` inside the repo directory. The resulting binary
will be available in `./target/release/k-means`.

Alternatively, you can use [static binary][3] which should work out of the box
on any x86_64 GNU/Linux system.

Running
=======

In order to test whether the binary works try sets in `test-datasets` folder.
The results should be like this:

    $ ./target/release/k-means --clusters=2 --threads=4 test-datasets/small.txt
    Data file read successfully, took 1 milliseconds
    Commencing K-Means clustering, searching 2 clusters in 4000 points
    K-Means converged after 2 iterations, took 0 milliseconds
    Cluster 0: centroid coordinates: [180.01707500000012, 18.29], cluster size: 800
    Cluster 1: centroid coordinates: [50.047634375000015, 8.82875], cluster size: 3200

License
=======

    Copyright (C) 2016-2019  Oleg `Kanedias` Chernovskiy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


[1]: https://en.wikipedia.org/wiki/K-means_clustering
[2]: https://rustup.rs/
[3]: https://gitlab.com/Kanedias/k-means-rust/-/jobs/artifacts/master/download?job=release+static